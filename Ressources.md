---
title: Quelques ressources pour R
author: GT Vallet
date: 2016-04-15
---

# R

- [Le site du projet](http://www.r-project.org/)
- [Répertoires des librairies R](http://cran.r-project.org/)
- [RStudio (IDE)](https://cran.r-project.org/doc/contrib/)
- [RSeek, moteur de recherche pour R](http://rseek.org/)

# Quelques ressources pour débuter

## Sites internet

- [Liste de ressources (Lyon 2)](http://eric.univ-lyon2.fr/~ricco/cours/cours_programmation_R.html)
- [Liste des contributions sur la doc pour R](https://cran.r-project.org/doc/contrib/)
- [Openclassrooms sur R](https://openclassrooms.com/courses/effectuez-vos-etudes-statistiques-avec-r)


## Documents PDF

- [Pour le débutant (E Paradis)](https://cran.r-project.org/doc/contrib/Paradis-rdebuts_fr.pdf)
- [Briser la glace](https://cran.r-project.org/doc/contrib/IceBreak_fr.pdf)
- [Livre sur R (V Goulet)](https://cran.r-project.org/doc/contrib/Goulet_introduction_programmation_R.pdf)
- [R en psychologie (en anglais)](https://cran.r-project.org/doc/contrib/Baron-rpsych.pdf)


## Tuto vidéos

- [LearnR on Youtube](https://www.youtube.com/channel/UCpcJNrQyW3Ge7w9-dmijW9Q)
Cf http://datascienceplus.com/learn-r-by-intensive-practice/
